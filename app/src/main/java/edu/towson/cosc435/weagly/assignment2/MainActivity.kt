package edu.towson.cosc435.weagly.assignment2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import edu.towson.cosc435.weagly.assignment2.ui.theme.Assignment2Theme
import java.math.RoundingMode
import java.text.DecimalFormat
import androidx.compose.material.OutlinedTextField as OutlinedTextField1

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Assignment2Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    TipCalcMain("Justin Weagly Tip Calculator")
                }
            }
        }
    }
}
//ASSIGNMENT 2 BY JUSTIN WEAGLY
@Composable
fun TipCalcMain(name: String) { //this is the main function for the tip calculator app

    var finalCost = remember { mutableStateOf("") } //stores the final
    var finalTip = remember { mutableStateOf("") } //stores the final
    var input = remember { mutableStateOf("") } //not sure if this should be a val or var
    var tipPercentage = remember { mutableStateOf("") } //holds the value for the tip percentage increase

    var resultText1 = remember { mutableStateOf("") } //these two variables hold the message that will be displayed once the user enters something
    var resultText2 = remember { mutableStateOf("") } //the message being “Your calculated tip is __ and your total is __”

    Column(horizontalAlignment = Alignment.CenterHorizontally) {

        Row( //title of app
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                fontSize = 28.sp,
                text = "\n\nTip Calculator\n\n"
            )
        }

        Row( //row for text telling user to enter text amount
            horizontalArrangement = Arrangement.Center
        ) {
            Text("Enter your check amount:\n\n")
        }

        Row(
            horizontalArrangement = Arrangement.Center
        ) {
            Text(fontSize = 28.sp,
                text = "$")
            OutlinedTextField1(
                keyboardOptions = KeyboardOptions //makes it so you can only type numbers
                    .Default
                    .copy(keyboardType = KeyboardType.Number, imeAction = ImeAction.Go),
                value = input.value , onValueChange = {newValue -> input.value = newValue})

        }

        Row(
            horizontalArrangement = Arrangement.Center
        ) {
            Text("\nSelect a tip percentage:\n\n")
        }

        Row( //Button 1 for 10%
            horizontalArrangement = Arrangement.Center
        ) {
            RadioButton(
                selected = (tipPercentage.value == "1.1"), //first button for 10 percent tip
                onClick = {tipPercentage.value = "1.1"}
            )
            Text(fontSize = 15.sp,
                text = "10%\n")
        }

        Row( //Button 2 for 20%
            horizontalArrangement = Arrangement.Center
        ) {
            RadioButton(
                selected = (tipPercentage.value == "1.2"), //second button for 20 percent tip
                onClick = {tipPercentage.value = "1.2"}
            )
            Text(fontSize = 15.sp,
                text = "20%\n")
        }

        Row( //Button 3 for 30%
            horizontalArrangement = Arrangement.Center
        ) {
            RadioButton(
                selected = (tipPercentage.value == "1.3"), //third button for 30 percent tip
                onClick = {tipPercentage.value = "1.3"}
            )
            Text(fontSize = 15.sp,
                text = "30%\n")
        }
        Button(onClick = { //the following things happen on click...
            finalCost.value = calculateTipTotal(input.value, tipPercentage.value);
            finalTip.value = calculateTip(input.value, tipPercentage.value);
            if(finalCost.value != "ERROR") {resultText1.value = "Your calculated tip is $"; resultText2.value = "\n and your total is $"}
            else if(finalCost.value == "ERROR") {resultText1.value = ""; resultText2.value = ""}
            //the above "if else if" combination creates clean error messages by erasing and rebuilding the final cost text depending on if there is an error or not.
        }) {
            Text("Calculate")
        }
        Text(
            fontSize = 28.sp, //this text displays the final result of the calculation
            text = resultText1.value + finalTip.value + resultText2.value + finalCost.value) //displays the final print message
    }
}

fun calculateTip(input: String, tipPercentage : String): String { //this returns just the tip that the user will be paying
    //return input
    try {

        val df = DecimalFormat("#.##") //this tool allows us to round the decimal to only two places, just as prices are displayed normally
        df.roundingMode = RoundingMode.DOWN

        return (((df.format(input.toDouble() * (tipPercentage.toDouble() - 1.0))).toString())) //calculates the tip and returns it as a string

    } catch (e: NumberFormatException) {  //throws an error if user does something stupid
        return("")
    }

}

fun calculateTipTotal(input: String, tipPercentage : String): String { //this returns the TOTAL price that the user will be paying after the tip
    //return input
    try {

        val df = DecimalFormat("#.##") //this tool allows us to round the decimal to only two places, just as prices are displayed normally
        df.roundingMode = RoundingMode.DOWN

        return (((df.format(input.toDouble() * tipPercentage.toDouble()))).toString()) //calculates the total price

    } catch (e: NumberFormatException) {  //throws an error if user does something stupid
        return("ERROR")
    }

}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Assignment2Theme {
        TipCalcMain("Justin Weagly Tip Calculator")
    }
}